﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace itadminportal.Classes
{
    public class Message : Entity
    {
        public DateTime SendDate { get; set; }
        public Contact Contact { get; set; }
        public string MessageContent { get; set; }
        public Task Task { get; set; }

        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(a => a.Id);
            builder.Property(a => a.SendDate).HasColumnType("datetime");
            builder.HasOne(x => x.Contact);
            builder.HasOne(x => x.Task);
        }
    }
}
