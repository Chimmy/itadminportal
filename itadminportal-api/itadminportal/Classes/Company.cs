﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace itadminportal.Classes
{
    public class Company : Entity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public virtual CreditCard CreditCard { get; set; }

        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(a => a.Id);
            builder.HasOne(x => x.CreditCard);
        }
    }
}
