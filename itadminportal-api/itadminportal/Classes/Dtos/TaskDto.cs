﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace itadminportal.Classes.Dtos
{
    public class TaskDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DueDate { get; set; }
        public TaskStatuses Status { get; set; }
        public User Technician { get; set; }
        public int TechnicianId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
