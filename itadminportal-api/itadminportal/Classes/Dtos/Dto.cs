﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace itadminportal.Classes.Dtos
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public string Claim { get; set; }

        public Contact Contact { get; set; }
    }

    public class MessageDto
    {
        public DateTime SendDate { get; set; }
        public virtual Contact Contact { get; set; }
        public string MessageContent { get; set; }
        public virtual Task Task { get; set; }
    }

    public class ContactDto
    {
        public int Id { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
