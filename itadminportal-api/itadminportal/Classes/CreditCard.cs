﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Data;

namespace itadminportal.Classes
{
    public class CreditCard : Entity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
    }
}
