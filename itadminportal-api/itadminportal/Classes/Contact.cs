﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Data;

namespace itadminportal.Classes
{
    public class Contact : Entity
    {
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
