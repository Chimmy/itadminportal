﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace itadminportal.Classes
{
    public class Task : Entity
    {
        public string Description { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DueDate { get; set; }
        public TaskStatuses Status { get; set; }
        public int TechnicianId{ get; set; }
        [ForeignKey("TechnicianId")]
        public virtual User Technician { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.HasKey(a => a.Id);
            builder.Property(a => a.UpdatedAt).HasColumnType("datetime");
            builder.Property(a => a.DueDate).HasColumnType("datetime");
            builder.HasOne(x => x.User);
            builder.HasOne(x => x.Technician);
            builder.Property(p => p.Status)
                .HasConversion(
                    x => x.ToString(),
                    x => (TaskStatuses)Enum.Parse(typeof(TaskStatuses), x));
        }

    }

    public enum TaskStatuses
    {
        Open,
        Doing,
        Done
    }
}
