﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace itadminportal.Classes
{
    public class User : Entity
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public string Claim { get; set; }
        public UserTypes UserType { get; set; } = UserTypes.User;

        public virtual Contact Contact { get; set; }

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(a => a.Id);
            builder.HasOne(x => x.Contact);
            builder.Property(p => p.UserType)
                .HasConversion(
                    x => x.ToString(),
                    x => (UserTypes) Enum.Parse(typeof(UserTypes), x));
        }
    }
}
