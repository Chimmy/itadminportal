﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces;
using itadminportal.Interfaces.Generic;
using itadminportal.Interfaces.Services;

namespace itadminportal.Services
{
    public class ContactService : GenericCRUDService<Contact, ContactDto>, IContactService
    {
        private readonly IRepository<Contact> contactRepository;
        public ContactService(
            IRepository<Contact> contactRepository,
            IMapper mapper
            ) : base(contactRepository, mapper)
        {
            
        }
    }
}
