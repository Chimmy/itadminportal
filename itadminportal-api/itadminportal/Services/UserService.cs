﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Controllers;
using itadminportal.Helpers;
using itadminportal.Interfaces;
using itadminportal.Interfaces.Generic;
using itadminportal.Interfaces.Services;
using Microsoft.EntityFrameworkCore;

namespace itadminportal.Services
{
    public class UserService : GenericCRUDService<User, UserDto>, IUserService
    {
        private readonly IRepository<User> userRepository;
        public UserService(IRepository<User> userRepository, IMapper mapper) : base(userRepository,mapper)
        {
            this.userRepository = userRepository;
        }

        public IEnumerable<UserDto> GetAllWithUserType(UserTechnicianSearchModel model)
        {
            var query = userRepository.GetAllFiltered(x => x.UserType == (UserTypes)model.Type);
            return query.ToList().Select(x => mapper.Map<UserDto>(x));
        }

        public User GetMe(int id)
        {
            return userRepository.GetContext().Include(x => x.Contact).FirstOrDefault(x => x.Id == id);
        }

        public UserDto GetUserForAdmin(int id)
        {
            return mapper.Map<UserDto>(userRepository.GetContext().Include(x => x.Contact).FirstOrDefault(x => x.Id == id));
        }

        public void UpdateMe(UserDto model, int id)
        {
            var me = userRepository.GetContext().Include(x => x.Contact).FirstOrDefault(x => x.Id == id);
            me.Name = model.Name;
            me.Surname = model.Surname;
            me.Contact.Email = model.Contact.Email;
            me.Contact.Telephone = model.Contact.Telephone;
            userRepository.Update(me);
        }

        public override UserDto Add(UserDto entityToAdd)
        {
            var user = mapper.Map<User>(entityToAdd);
            user.Password = HashHelper.GetHashString(user.Password);
            user.Claim = entityToAdd.Claim.First().ToString().ToUpper() + entityToAdd.Claim.Substring(1);
            return mapper.Map<UserDto>(repository.Add(user));
        }
    }
}
