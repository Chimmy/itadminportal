﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Classes.Dtos;
using itadminportal.Data;

namespace itadminportal.Interfaces.Generic
{
    public class GenericCRUDService<TEntity, TDto> 
        : IGenericCRUDService<TEntity,TDto>
        where TEntity : class, IEntity, new()
        where TDto : class
    {
        protected readonly IRepository<TEntity> repository;
        protected readonly IMapper mapper;
        public GenericCRUDService(IRepository<TEntity> repository, IMapper mapperz)
        {
            this.repository = repository;
            this.mapper = mapperz;
        }


        public virtual IQueryable<TDto> GetAll()
        {
            return repository.GetAll().Select(x => mapper.Map<TDto>(x));
        }

        public virtual IQueryable<TDto> GetAllFiltered(Predicate<TEntity> filter)
        {
            return repository.GetAllFiltered(filter).Select(x => mapper.Map<TDto>(x));
        }

        public virtual TDto Get(int id)
        {
            return mapper.Map<TDto>(repository.Get(id));
        }

        public virtual TDto FirstOrDefault(Predicate<TEntity> pred)
        {
            return mapper.Map<TDto>(repository.FirstOrDefault(pred.Invoke));
        }

        public virtual TDto Add(TDto entityToAdd)
        {
            return mapper.Map<TDto>(repository.Add(mapper.Map<TEntity>(entityToAdd)));
        }

        public virtual void Delete(int entityToDeleteId)
        {
            repository.Delete(entityToDeleteId);
        }

        public virtual void Update(TDto entityToUpdate, int id)
        {
            var ent = new TEntity();
            mapper.Map(entityToUpdate, ent);
            ent.Id = id;
            repository.Update(ent);
        }

        public virtual void UpdateRange(IEnumerable<TDto> listToUpdate)
        {
            repository.UpdateRange(listToUpdate.Select(e => mapper.Map<TEntity>(e)));

        }

    }
}
