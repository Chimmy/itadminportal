﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces;
using itadminportal.Interfaces.Generic;
using itadminportal.Interfaces.Services;

namespace itadminportal.Services
{
    public class MessageService : GenericCRUDService<Message, MessageDto>, IMessageService
    {
        private readonly IRepository<Message> messageRepository;
        public MessageService(IRepository<Message> messageRepository, IMapper mapper) : base(messageRepository, mapper)
        {
            this.messageRepository = messageRepository;
        }
    }
}
