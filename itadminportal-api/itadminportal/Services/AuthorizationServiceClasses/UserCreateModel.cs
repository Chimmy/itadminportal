﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace itadminportal.Services.AuthorizationServiceClasses
{
    public class UserCreateModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Claim { get; set; }

    }
}
