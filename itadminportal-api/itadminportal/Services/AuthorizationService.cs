﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Controllers;
using itadminportal.Data;
using itadminportal.Exceptions;
using itadminportal.Helpers;
using itadminportal.Interfaces;
using itadminportal.Interfaces.Services;
using itadminportal.Services.AuthorizationServiceClasses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace itadminportal.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private IRepository<User> userRepository;
        private IConfiguration cfg;

        public AuthorizationService(IRepository<User> userRepository, IConfiguration cfg)
        {
            this.userRepository = userRepository;
            this.cfg = cfg;
        }
        private bool IsAuthorized(LoginModel model, out User user)
        {
            user = userRepository.FirstOrDefault(x => x.Username == model.UserName);
            if (user == null)
            {
                throw new UserNotFoundException();
            }

            return user.Password == HashHelper.GetHashString(model.Password);
        }

        public string GetToken(LoginModel model)
        {
            if (IsAuthorized(model, out var user))
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(cfg["Secret"]));

                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var claims = new List<Claim>
                {
                    new Claim("name", user.Username),
                    new Claim("role", user.Claim),
                    new Claim("userid", user.Id.ToString())
                };

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5000",
                    audience: "http://localhost:5000",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(5),
                    signingCredentials: signinCredentials
                    
                );

                return new JwtSecurityTokenHandler().WriteToken(tokeOptions);
            }

            return null;
        }

        public void CreateUser(AuthController.CreateUserDto model)
        {
            userRepository.Add(new User
            {
                Username = model.Username,
                Password = HashHelper.GetHashString(model.Password),
                Claim = model.Claim
            });
        }

        public bool CreateUser(UserCreateModel model)
        {
            var result = false;
            try
            {
                userRepository.Add(new User
                {
                    Username = model.Username,
                    Password = HashHelper.GetHashString(model.Password),
                    Claim = model.Claim
                });
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }

            return result;

        }
    }
}
