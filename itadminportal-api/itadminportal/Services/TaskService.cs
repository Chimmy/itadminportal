﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces;
using itadminportal.Interfaces.Generic;
using itadminportal.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using Task = itadminportal.Classes.Task;

namespace itadminportal.Services
{
    public class TaskService : GenericCRUDService<Task, TaskDto>, ITaskService
    {
        private readonly IRepository<Task> taskRepository;
        private readonly IRepository<Message> messageRepository;
        //private readonly IContactService contactService;
        private readonly IRepository<User> userRepository;
        public TaskService(
            IRepository<Task> taskRepository,
            IRepository<User> userRepository,
            IRepository<Message> messageRepository,
            IMapper mapper) 
            : base(taskRepository, mapper)
        {
            this.taskRepository = taskRepository;
            this.userRepository = userRepository;
            this.messageRepository = messageRepository;
        }

        public override TaskDto Add(TaskDto entityToAdd)
        {
            var task = repository.Add(mapper.Map<Task>(entityToAdd));
            if (task != null)
            {
                var user = userRepository.GetAllFiltered(x => x.Id == entityToAdd.UserId)
                    .Include(x => x.Contact)
                    .FirstOrDefault();
                var message = new Message
                {
                    Contact = user.Contact,
                    MessageContent = $"Task with id :{task.Id} is due!",
                    SendDate = task.DueDate,
                    Task = task
                };

                messageRepository.Add(message);
            }

            return mapper.Map<TaskDto>(task);
        }

        public override TaskDto Get(int id)
        {
            var taskCtx = taskRepository.GetContext();
            var task = taskCtx
                .Include(x => x.User)
                .Include(x => x.User.Contact)
                .Include(x => x.Technician)
                .Include(x => x.Technician.Contact).FirstOrDefault(x => x.Id == id);
            var taskDto = mapper.Map<TaskDto>(task);

            return taskDto;
        }
    }
}
