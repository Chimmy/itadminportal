﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using itadminportal.Data;
using itadminportal.Interfaces;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace itadminportal.Generic
{
    public class Repository<TEntity> : IRepository<TEntity> 
        where TEntity : class, IEntity 
        
    {
        internal AdminPortalContext ctx;
        internal DbSet<TEntity> dbSet;

        public Repository(AdminPortalContext ctx)
        {
            this.ctx = ctx;
            this.dbSet = ctx.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return dbSet;
        }

        public IQueryable<TEntity> GetAllFiltered(Predicate<TEntity> filter )
        {
            return dbSet.Where(filter.Invoke).AsQueryable();
        }

        public DbSet<TEntity> GetContext()
        {
            return this.dbSet;
        }

        public TEntity Get(int id)
        {
            return dbSet.Find(id);
        }

        public TEntity FirstOrDefault(Predicate<TEntity> pred)
        {
            return dbSet.FirstOrDefault(pred.Invoke);
        }

        public TEntity Add(TEntity entityToAdd)
        {
            var res = dbSet.Add(entityToAdd);
            this.ctx.SaveChanges();
            return res.Entity;
        }

        public void Delete(int entityToDeleteId)
        {
            var ent = dbSet.Find(entityToDeleteId);
            if (ent != null)
            {
                dbSet.Remove(ent);
            }
            ctx.SaveChanges();
        }

        public void Update(TEntity entityToUpdate)
        {
            dbSet.Update(entityToUpdate);
            ctx.SaveChanges();
        }

        public void UpdateRange(IEnumerable<TEntity> listToUpdate)
        {
            dbSet.UpdateRange(listToUpdate);
            ctx.SaveChanges();
        }

        public TEntity Get(int id, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = dbSet.Where(x => x.Id == id);
            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }

            return query.FirstOrDefault();
        }

        public IEnumerable<TEntity> GetAll(Predicate<TEntity> filter = null, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = dbSet.AsQueryable();
            if (filter != null)
            {
                query.Where(filter.Invoke);
            }

            query = includes.Aggregate(query, (current, include) => current.Include(include));
            return query.ToList();
        }
    }
}
