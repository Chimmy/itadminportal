﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces.Services;
using itadminportal.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace itadminportal.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService taskService;
        

        public TaskController(ITaskService taskService)
        {
            this.taskService = taskService;
        }
        [HttpGet]
        public IEnumerable<TaskDto> GetAll()
        {
            return taskService.GetAll();
        }

        [HttpGet("{id}")]
        public TaskDto Get(int id)
        {
            return taskService.Get(id);
        }

        // POST: api/Task
        [HttpPost]
        public ActionResult<TaskDto> Post([FromBody] TaskDto dto)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            dto.UserId = int.Parse(identity.FindFirst("userid").Value);
            return Created(nameof(TaskService), taskService.Add(dto));
        }

        // PUT: api/Task/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] TaskDto value)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            value.UserId = int.Parse(identity.FindFirst("userid").Value);
            value.UpdatedAt = DateTime.Now;
            taskService.Update(value, id);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            taskService.Delete(id);
            return Ok();
        }
    }
}
