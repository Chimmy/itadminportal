﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Exceptions;
using itadminportal.Interfaces;
using itadminportal.StaticStrings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using IAuthorizationService = itadminportal.Interfaces.Services.IAuthorizationService;

namespace itadminportal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IAuthorizationService AuthService;
        
        public AuthController(IAuthorizationService authService)
        {
            AuthService = authService;
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody] LoginModel user)
        {
            try
            {
                var tokenString = AuthService.GetToken(user);
                if (tokenString == null)
                {
                    return Unauthorized("Credentials invalid");
                }

                return Ok(new { Token = tokenString });
            }
            catch (Exception e)
            {
                if (e is UserNotFoundException)
                {
                    return NotFound("User not in database");
                }

                throw e;
            }
        }
        

        [HttpPost, Route("createuser"), Authorize(Roles = "Admin")]
        public IActionResult Create([FromBody] CreateUserDto info)
        {
            AuthService.CreateUser(info);
            return Ok();
        }

        //[HttpPost, Route("usertypes"), Authorize(Roles = "Admin")]
        //public IActionResult Create([FromBody] CreateUserDto info)
        //{
        //    AuthService.CreateUser(info);
        //    return Ok();
        //}

        public class CreateUserDto
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Claim { get; set; }
        }
      
    }
}
