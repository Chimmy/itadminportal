﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces.Services;
using itadminportal.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace itadminportal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }
        // GET: api/User
        [HttpGet, Authorize(Roles="Admin")]
        public IEnumerable<UserDto> GetAll()
        {
            return userService.GetAll();
        }

        [HttpPost, Route("usersoftype") , Authorize(Roles = "User,Technician,Admin")]
        public IEnumerable<UserDto> GetUsersOfType(UserTechnicianSearchModel model)
        {
            return userService.GetAllWithUserType(model);
        }

        [HttpGet("{id}"), Authorize]
        public UserDto Get(int id)
        {
            var identity = (HttpContext.User.Identity as ClaimsIdentity);
            var role = identity.FindFirst("http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            var isAdmin = role == "Admin";
            if (isAdmin)
            {
                return userService.GetUserForAdmin(id);
            }
            return userService.Get(id);
        }
        [HttpPost, Route("me"), Authorize]
        public IActionResult UpdateMe(UserDto model)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            var id = int.Parse(identity.FindFirst("userid").Value);
            userService.UpdateMe(model, id);
            return Ok();
        }

        // POST: api/Task
        [HttpPost, Authorize]
        public ActionResult<UserDto> Post([FromBody] UserDto dto)
        {
            return Created(nameof(TaskService), userService.Add(dto));
        }

        [HttpGet, Route("me"), Authorize]
        public ActionResult<UserDto> Get()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            var id = int.Parse(identity.FindFirst("userid").Value);
            return Ok(userService.GetMe(id));
        }

        // PUT: api/Task/5
        [HttpPut("{id}"), Authorize]
        public IActionResult Put(int id, [FromBody] UserDto value)
        {
            userService.Update(value, id);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}"), Authorize]
        public IActionResult Delete(int id)
        {
            userService.Delete(id);
            return Ok();
        }
    }
}
