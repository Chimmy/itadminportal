﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Helpers;
using itadminportal.StaticStrings;
using Microsoft.CodeAnalysis.FlowAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Task = itadminportal.Classes.Task;

namespace itadminportal.Data
{
    public class AdminPortalContext : DbContext
    {
        private readonly IConfiguration Configuration;
        public AdminPortalContext(IConfiguration cfg) : base()
        {
            Configuration = cfg;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration["ConnectionString"]);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var a in modelBuilder.Model.FindEntityType(typeof(Task)).Model.GetEntityTypes().SelectMany(x => x.GetForeignKeys()))
            {
                a.DeleteBehavior = DeleteBehavior.NoAction;
            }
            modelBuilder.Entity<Contact>().HasData(
                new
                {
                    Email = "admin@gmail.com",
                    Id = 1,
                    Telephone = "8686888888"
                },
                new
                {
                    Email = "tech@gmail.com",
                    Id = 2,
                    Telephone = "8686888888"
                },
                new
                {
                    Email = "user@gmail.com",
                    Id = 3,
                    Telephone = "8686888888"
                }
            );
            modelBuilder.Entity<User>().HasData(
                new
                {
                    Id = 1,
                    Username = "admin",
                    Name = "System",
                    Surname = "Administrator",
                    Password = HashHelper.GetHashString("admin"),
                    Claim = Roles.Admin,
                    UserType = UserTypes.User,
                    ContactId = 1
                },
                new
                {
                    Id = 2,
                    Username = "technician",
                    Name = "Paulius",
                    Surname = "Adomaitis",
                    Password = HashHelper.GetHashString("admin"),
                    Claim = Roles.Technician,
                    UserType = UserTypes.Technician,
                    ContactId = 2
                },
                new
                {
                    Id = 3,
                    Username = "user",
                    Name = "Pranas",
                    Surname = "Pranaitis",
                    Password = HashHelper.GetHashString("admin"),
                    Claim = Roles.User,
                    UserType = UserTypes.User,
                    ContactId = 3
                });
         
            base.OnModelCreating(modelBuilder);
        }
    }
}
