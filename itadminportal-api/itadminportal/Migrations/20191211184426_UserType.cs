﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace itadminportal.Migrations
{
    public partial class UserType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "UserType",
                value: 1);

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Claim", "ContactId", "Name", "Password", "Surname", "UserType", "Username" },
                values: new object[] { 3, "User", null, "System", "8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918", "Administrator", 1, "user" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Claim", "ContactId", "Name", "Password", "Surname", "UserType", "Username" },
                values: new object[] { 2, "Technician", null, "System", "8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918", "Administrator", 0, "technician" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DropColumn(
                name: "UserType",
                table: "Users");
        }
    }
}
