﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace itadminportal.Interfaces.Generic
{
    public interface IGenericCRUDService<TEntity, TDto>
    {
        IQueryable<TDto> GetAll();
        IQueryable<TDto> GetAllFiltered(Predicate<TEntity> filter);
        TDto Get(int id);
        TDto FirstOrDefault(Predicate<TEntity> pred);
        TDto Add(TDto entityToAdd);
        void Delete(int entityToDeleteId);
        void Update(TDto entityToUpdate, int id);
        void UpdateRange(IEnumerable<TDto> listToUpdate);
    }
}
