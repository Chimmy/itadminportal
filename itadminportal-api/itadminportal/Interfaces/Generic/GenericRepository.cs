﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace itadminportal.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAllFiltered(Predicate<TEntity> filter);
        TEntity Get(int id);
        TEntity FirstOrDefault(Predicate<TEntity> pred);
        TEntity Add(TEntity entityToAdd);
        void Delete(int entityToDeleteId);
        void Update(TEntity entityToUpdate);
        void UpdateRange(IEnumerable<TEntity> listToUpdate);
        DbSet<TEntity> GetContext();
        TEntity Get(int id, params Expression<Func<TEntity, object>>[] includes);
        IEnumerable<TEntity> GetAll(Predicate<TEntity> filter = null,
            params Expression<Func<TEntity, object>>[] includes);
    }
}
