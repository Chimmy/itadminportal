﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces.Generic;

namespace itadminportal.Interfaces.Services
{
    public interface IMessageService : IGenericCRUDService<Message, MessageDto>
    {
    }
}
