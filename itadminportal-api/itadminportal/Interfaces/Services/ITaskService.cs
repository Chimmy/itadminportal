﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces.Generic;
using Task = itadminportal.Classes.Task;

namespace itadminportal.Interfaces.Services
{
    public interface ITaskService : IGenericCRUDService<Task, TaskDto>
    {
    }

    public interface IContactService : IGenericCRUDService<Contact, ContactDto>
    {
    }
}
