﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace itadminportal.Interfaces.Services
{
    public interface IAuthorizationService
    {
        string GetToken(LoginModel model);
        void CreateUser(AuthController.CreateUserDto model);
    }
}
