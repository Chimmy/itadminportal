﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;
using itadminportal.Interfaces.Generic;

namespace itadminportal.Interfaces.Services
{
    public interface  IUserService : IGenericCRUDService<User, UserDto>
    {
        IEnumerable<UserDto> GetAllWithUserType(UserTechnicianSearchModel model);

        User GetMe(int id);

        void UpdateMe(UserDto model, int id);

        UserDto GetUserForAdmin(int id);
    }


}
