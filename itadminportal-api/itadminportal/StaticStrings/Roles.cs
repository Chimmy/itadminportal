﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace itadminportal.StaticStrings
{
    public static class Roles
    {
        public static string Admin = "Admin";
        public static string User = "User";
        public static string Technician = "Technician";

        public static List<string> ValidClaims = new List<string>
        {
            Admin,
            User,
            Technician
        };
    }
}
