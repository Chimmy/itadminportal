﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using itadminportal.Classes;
using itadminportal.Classes.Dtos;

namespace itadminportal.Helpers
{
    public class AdminPortalMapperProfile : Profile
    {
        public AdminPortalMapperProfile()
        {
            this.AllowNullDestinationValues = true;
            CreateMap<Task, TaskDto>()
                //.ForMember(src => src.Technician, opt => opt.MapFrom(x => x.Technician))
                //.ForMember(src => src.User, opt => opt.MapFrom(opt => opt.User))
                //.ForMember(src => src.Description, opt => opt.MapFrom(x => x.Description))
                //.ForMember(src => src.DueDate, opt => opt.MapFrom(x => x.DueDate))
                //.ForMember(src => src.Status, opt => opt.MapFrom(x => x.Status))
                //.ForMember(src => src.UserId, opt => opt.MapFrom(x => x.UserId))
                //.ForMember(src => src.Id, opt => opt.MapFrom(x => x.Id))
                //.ForMember(src => src.TechnicianId, opt => opt.MapFrom(x => x.TechnicianId))
                .ReverseMap();

            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Contact, ContactDto>().ReverseMap();
            CreateMap<Message, MessageDto>().ReverseMap();
            //CreateMap<User, UserDto>().ReverseMap();
        }
    }
}
