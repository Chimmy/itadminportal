using System;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoMapper;
using itadminportal.Data;
using itadminportal.Generic;
using itadminportal.Helpers;
using itadminportal.Interfaces;
using itadminportal.Interfaces.Generic;
using itadminportal.Interfaces.Services;
using itadminportal.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace itadminportal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(opt =>
            {
                opt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "http://localhost:5000",
                    ValidAudience = "http://localhost:5000",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Secret"]))
                };
            });
            services.AddDbContext<AdminPortalContext>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddControllers();
            services.AddCors(opt =>
            {
                opt.AddPolicy("EnableCors", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
            //foreach (var subClass in Assembly.GetAssembly(typeof(Entity)).GetTypes()
            //    .Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(typeof(Entity))))
            //{
            //    services.Add(new ServiceDescriptor(typeof(IRepository<>), typeof(Repository<subClass>), ServiceLifetime.Transient));
            //}
            var mappingCfg = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AdminPortalMapperProfile());
            });


            services.AddSingleton<IMapper, Mapper>(sp => (Mapper) mappingCfg.CreateMapper());
            //services.AddTransient<IMapper, Mapper>();
            services.Add(new ServiceDescriptor(typeof(IRepository<>), typeof(Repository<>), ServiceLifetime.Transient));
            //services.Add(new ServiceDescriptor(typeof(IGenericCRUDService<>), typeof(GenericCRUDService<>), ServiceLifetime.Transient));
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<IAuthorizationService, AuthorizationService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IContactService, ContactService>();
            services.AddTransient<IMessageService, MessageService>();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "IT admin portal",
                    Version = "v1"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseCors("EnableCors");

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        }
    }
}