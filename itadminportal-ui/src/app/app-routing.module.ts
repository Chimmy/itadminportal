import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainWindowComponent } from './general/main-window/main-window.component';
import { DocumentComponent } from './pages/document/document.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuardService } from './services/guards/auth-guard.service';
import { CreateTaskComponent } from './pages/create-task/create-task.component';
import { ViewTaskComponent } from './pages/view-task/view-task.component';
import { ViewTasksComponent } from './pages/view-tasks/view-tasks.component';
import { ProfileOtherComponent } from './general/profile-other/profile-other.component';
import { ManagementComponent } from './general/management/management.component';


const routes: Routes = [{
    path: '',
    component: MainWindowComponent,
    pathMatch: 'full',
    canActivate: [AuthGuardService],
    data: {animation: 'mainWindow'}
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {animation: 'login'}
  }, {
    path: 'document',
    component: DocumentComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'document'}
  }, {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'profile'}
  },
  {
    path: 'task-create',
    component: CreateTaskComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'taskcreate'}
  },
  {
    path: 'task/:id',
    component: ViewTaskComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'taskview'}
  },
  {
    path: 'task-view',
    component: ViewTasksComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'tasks'}
  },
  {
    path: 'management',
    component: ManagementComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'management'}
  },
  {
    path: 'profile/:id',
    component: ProfileOtherComponent,
    canActivate: [AuthGuardService],
    data: {animation: 'profileone'}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
