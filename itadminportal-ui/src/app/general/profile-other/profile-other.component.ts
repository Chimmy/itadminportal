import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { JwtHelperService } from '@auth0/angular-jwt';
import { IFullUser } from 'src/app/pages/view-task/view-task.component';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile-other',
  templateUrl: './profile-other.component.html',
  styleUrls: ['./profile-other.component.scss']
})
export class ProfileOtherComponent implements OnInit {
  user: IFullUser;
  initObservable: Observable<any>;
  profileOtherForm: FormGroup;
  createUserForm: FormGroup;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private jwtHelper: JwtHelperService
  ) { 
    this.profileOtherForm = this.formBuilder.group({
      name : ['', Validators.required],
      surname : ['', Validators.required],
      email: ['', Validators.required],
      telephone: ['',Validators.required]
    });

    this.createUserForm = this.formBuilder.group({
      password : ['', Validators.required],
      claim : ['User', Validators.required],
      username : ['', Validators.required],
      name : ['', Validators.required],
      surname : ['', Validators.required],
      email: ['', Validators.required],
      telephone: ['',Validators.required]
    });
  }

  ngOnInit() {
    this.initObservable = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const id = params.get('id');
        return this.http.get(environment.apiUrl + `/api/user/${id}`);
      })
    );
    this.init();
  }
  init() {
    this.initObservable.subscribe(r => {
      this.user = r;
      this.profileOtherForm.controls['name'].setValue(this.user.name);
      this.profileOtherForm.controls['surname'].setValue(this.user.surname);
      this.profileOtherForm.controls['email'].setValue(this.user.contact.email);
      this.profileOtherForm.controls['telephone'].setValue(this.user.contact.telephone);
      console.log(this.user);
    })
  }

  change() {
    let formval = this.profileOtherForm.value;
    let dto = <any> {
      name: formval.name,
      surname: formval.surname,
      contact: {
        email: formval.email,
        telephone: formval.telephone
      }
    }
    this.http.post(environment.apiUrl + '/api/user/me', dto).subscribe(() => {

      this.snackBar.open('User updated!', 'Ok', {
        duration: 1000
      });
      this.router.navigate(['management']);
    })
  }

  delete() {
    this.http.delete(environment.apiUrl + `/api/user/${this.user.id}`).subscribe(() => {
      this.snackBar.open('User deleted!', 'Ok', {
        duration: 1000
      });
      this.router.navigate(['management']);
    });
  }

  create() {
    let formval = this.createUserForm.value;
    let dto = <any> {
      username: formval.username,
      name: formval.name,
      surname: formval.surname,
      password: formval.password,
      claim: formval.claim,
      contact: {
        email: formval.email,
        telephone: formval.telephone
      }
    }
    this.http.post(environment.apiUrl + '/api/user', dto).subscribe(() => {
      this.snackBar.open('User created!', 'Ok', {
        duration: 1000
      });
      this.router.navigate(['management']);
    });
  }
}
