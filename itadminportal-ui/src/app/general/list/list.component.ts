import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  showList = false;
  @Input() input: Subject<any>;
  @Output() itemClicked: EventEmitter<any> = new EventEmitter<any>();
  data: DynamicDataSource;
  displayedColumns: Array<string> = [];
  columns: Array<any & IHaveId>;
  routerSubscription: Subscription;
  constructor(private router: Router) { }

  ngOnInit() {
    this.columns = [];
    this.formatData();
    this.routerSubscription = this.router.events.subscribe((e) => {
      if(e instanceof NavigationStart) {
        this.showList = false;
      }
    });
  }

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }

  executeAction(data: any & IHaveId) {
    this.itemClicked.next(data);
  }

  formatData() {
    this.input.subscribe((data: Array<any & IHaveId>) => {
      if (data && data !== null) {
        this.showList = true;
        this.data = new DynamicDataSource(data);
        this.displayedColumns = Object.keys(data[0]).map(key => key).filter(x => x !== 'id');

        for (const key of Object.keys(data[0])) {
          this.columns.push({
            columnDef: key,
            header: this.extractDataName(key),
            cell: (element: any) => element[key],
          });
        }
      }
    });
  }

  extractDataName(entry: string) {
    return entry.charAt(0).toUpperCase() + entry.slice(1).replace('_', ' ');
  }
}

export interface IHaveId {
  id: number;
}

export class DynamicDataSource extends DataSource<any> {
  public data: Array<any & IHaveId>;
  constructor(data: Array<any & IHaveId>) {
    super();
    this.data = data;

  }
  connect(): Observable<Element[]> {
    return of(this.data);
  }

  disconnect() {}
}
