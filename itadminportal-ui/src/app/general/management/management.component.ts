import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { flatMap } from 'rxjs/operators';
import { IFullUser } from 'src/app/pages/view-task/view-task.component';
import { of } from 'rxjs';
import { IHaveId } from '../list/list.component';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {
  data: any;
  role: string;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private jwtHelper: JwtHelperService
  ) { }

  ngOnInit() {
    this.role = this.jwtHelper.decodeToken(this.jwtHelper.tokenGetter()).role;
    if(this.role === 'Admin') {
      this.data = this.http.get(environment.apiUrl + '/api/user').pipe(flatMap((r: IFullUser[]) => {
        return of(r.map(x => <any>{
          id: x.id,
          name: x.name,
          surname: x.surname,
        }));
      }));
    }
  }

  itemClicked($event: IHaveId) {
    this.router.navigate(['profile', $event.id]);
  }

}
