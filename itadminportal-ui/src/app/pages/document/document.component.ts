import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import mockData from '../../assets/profit.json';
import { Observable, of, Subject, PartialObserver } from 'rxjs';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  generationCriteria: FormGroup;
  data: Subject<any> = new Subject<any>();
  constructor(private formBuilder: FormBuilder) {
    this.generationCriteria = this.formBuilder.group({
      from : [new Date()],
      to : [new Date()],
      type: ['', Validators.required]
    });
   }

  ngOnInit() {
  }

  generate() {
    console.log(mockData);
    this.data.next(mockData);
  }

  private getLabels() {

  }

  itemClicked($event) {
    console.log($event);
  }
}
