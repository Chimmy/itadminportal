import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IHaveId } from 'src/app/general/list/list.component';
import { flatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { IFullTask } from '../view-task/view-task.component';
import { Statuses } from '../create-task/create-task.component';

@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.scss']
})
export class ViewTasksComponent implements OnInit {
  data: any;
  constructor(private http: HttpClient, private router: Router) {
    this.data = this.http.get(environment.apiUrl + '/api/task').pipe(flatMap((r: IFullTask[]) => {
      console.log(r);
      return of(r.map(x => <any>{
        id: x.id,
        description: x.description,
        due_date: x.dueDate,
        updated_at: x.updatedAt,
        status: Statuses[x.status],
      }));
    }));
   }

  ngOnInit() {
  }

  itemClicked($event: IHaveId) {
    this.router.navigate(['task', $event.id]);
  }
}
