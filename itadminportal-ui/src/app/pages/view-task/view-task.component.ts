import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Statuses, IUser, UserTypes } from '../create-task/create-task.component';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { switchMap, tap, concatMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Observable, of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.scss']
})
export class ViewTaskComponent implements OnInit {
  private task: ITask;
  private technician: IUser;
  private technicians: Array<IUser>;
  private user: IUser;
  private crudTask: FormGroup;
  private initObservable: Observable<any>;
  private role: string;
  private statusOptions: {} = [
    {
      display: 'Ready',
      status: Statuses.Ready
    },
    {
      display: 'In progress',
      status: Statuses.InProgress
    },
    {
      display: 'Done',
      status: Statuses.Done
    },
  ];
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private jwtHelper: JwtHelperService
    ) {
      this.crudTask = this.formBuilder.group({
      dueDate : [Date()],
        description : [''],
        technicianId: [Number(null), Validators.required],
        status: [Statuses.Ready]
      });
  }

  ngOnInit() {
    this.role = this.jwtHelper.decodeToken(this.jwtHelper.tokenGetter()).role;
    this.http.post(environment.apiUrl + '/api/user/usersoftype', {type: UserTypes.Technician})
    .subscribe((r: Array<IUser>) => {
      this.technicians = r;
    });

    this.initObservable = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const id = params.get('id');
        return this.http.get(environment.apiUrl + `/api/task/${id}`);
      })
    );
    this.init();
  }

  private init() {
    this.initObservable.subscribe((r: IFullTask) => {
      this.technician = r.technician;
      this.user = r.user
      this.task = r;
      this.crudTask.controls['dueDate'].setValue(this.task.dueDate);
      this.crudTask.controls['description'].setValue(this.task.description);
      this.crudTask.controls['technicianId'].setValue(this.task.technicianId);
      this.crudTask.controls['status'].setValue(this.task.status);
    
    });
  }

  update() {
    this.http.put(environment.apiUrl + `/api/task/${this.task.id}`, this.crudTask.value).subscribe(() => {
      this.snackBar.open('Task updated!', 'Ok', {
        duration: 1000
      });
      this.init();
    });
  }

  delete() {
    this.http.delete(environment.apiUrl + `/api/task/${this.task.id}`).subscribe(() => {
      this.snackBar.open('Task updated!', 'Ok', {
        duration: 1000
      });
      this.init();
    });
  }
}

export interface IFullTask {
  id: number;
  description: string;
  dueDate: Date;
  updatedAt: Date;
  status: Statuses;
  technician: IFullUser;
  technicianId: number;
  user: IFullUser;
  userId: number;
}

export interface IFullUser extends IUser {
  claim:string;
  surname: string;
  userType: UserTypes;
  username: string;
  contact: IContact;
}

export interface IContact {
  telephone:string;
  email:string
}

export interface ITask {
  id: number;
  description: string;
  dueDate: Date;
  updatedAt: Date;
  status: Statuses;
  technicianId: number;
  userId: number;
}
