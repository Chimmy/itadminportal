import { Component, OnInit } from '@angular/core';
import { IUser } from '../create-task/create-task.component';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IFullUser } from '../view-task/view-task.component';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { of } from 'rxjs';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  me: IFullUser;
  profileForm: FormGroup;
  role:string;
  data: any;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private jwtHelper: JwtHelperService
  ) {
    this.profileForm = this.formBuilder.group({
      name : ['', Validators.required],
      surname : ['', Validators.required],
      email: ['', Validators.required],
      telephone: ['',Validators.required]
    });
   }

  ngOnInit() {
    this.role = this.jwtHelper.decodeToken(this.jwtHelper.tokenGetter()).role;
    this.http.get(environment.apiUrl + '/api/user/me').subscribe((r: IFullUser) => {
      this.me = r;
      this.profileForm.controls['name'].setValue(this.me.name);
      this.profileForm.controls['surname'].setValue(this.me.surname);
      this.profileForm.controls['email'].setValue(this.me.contact.email);
      this.profileForm.controls['telephone'].setValue(this.me.contact.telephone);
    });
  }

  change() {
    let formval = this.profileForm.value;
    let dto = <any> {
      name: formval.name,
      surname: formval.surname,
      contact: {
        email: formval.email,
        telephone: formval.telephone
      }
    }
    this.http.post(environment.apiUrl + '/api/user/me', dto).subscribe(() => {

      this.snackBar.open('Your date has been updated!', 'Ok', {
        duration: 1000
      });
    })
  }

}
