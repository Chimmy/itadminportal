import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {
  technicians: any;
  taskForm: FormGroup;
  constructor(private http: HttpClient, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
    this.taskForm = this.formBuilder.group({
      dueDate : [new Date()],
      description : [''],
      technicianId: [Number(null), Validators.required],
      status: [Statuses.Ready]
    });
    this.http.post(environment.apiUrl + '/api/user/usersoftype', {type: UserTypes.Technician})
    .subscribe(r => {
      console.log(r);
      this.technicians = r;
    });
  }

  create() {
    this.http.post(environment.apiUrl + '/api/task', this.taskForm.value).subscribe(() => {
      this.snackBar.open('Task created!', 'Ok', {
        duration: 1000
      });
      this.router.navigate(['task-view'])
    });
  }

}

export interface ITaskCreate {
  description: string;
  dueDate: Date;
  status: Statuses;
  technicianId: number;
}

export enum Statuses {
  Ready,
  InProgress,
  Done
}

export enum UserTypes {
  Technician,
  User
}

export interface IUser  {
  id:number;
  name: string;
}
