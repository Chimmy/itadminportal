import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormFactoryService {

  constructor(private formBuilder: FormBuilder) { }

  generateForm(
    inputs: Array<string>,
    dates: Array<string>
  ) {
    const controlConfigs = {};
    inputs.map(x => controlConfigs[x]);
    return this.formBuilder.group(controlConfigs);
  }
}
