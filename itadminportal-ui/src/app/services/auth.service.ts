import { Injectable } from '@angular/core';
import { UserModel } from '../general/models/login-model';
import { environment } from 'src/environments/environment';
import { Observable, of, Subject } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, flatMap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedIn: Subject<boolean> = new Subject<boolean>();
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) { }

  public authenticate(model: UserModel): Observable<string> {
    const authUrl = `${environment.apiUrl}/api/auth/login`;
    return this.http.post(authUrl, model).pipe(
      flatMap((r: HttpResponse<Token>): Observable<string> => {
        // @ts-ignore
        localStorage.setItem('token', r.token);
        this.isLoggedIn.next(true);
        // @ts-ignore
        return of(r.token);
    }));
  }

  public logOut = () => {
    localStorage.removeItem("token");
    this.isLoggedIn.next(false);
  }

  public isAuthenticated() {
    let token: string = localStorage.getItem("token");
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    else {
      return false;
    }
  }
}

interface Token {
  token: string;
}
