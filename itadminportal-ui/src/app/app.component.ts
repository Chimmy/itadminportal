import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slide } from './animations';
import { JwtHelperService } from '@auth0/angular-jwt';
import { of } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slide]
})
export class AppComponent {
  title = 'rideshare-ui';
  role: string;
  authenticated: boolean;
  constructor(
    private jwtHelper: JwtHelperService,
    private auth: AuthService
  ) {
    this.role = this.jwtHelper.decodeToken(this.jwtHelper.tokenGetter()).role;
    auth.isLoggedIn.subscribe(r => {
      this.authenticated = r;
    })
    // of(this.jwtHelper.tokenGetter().length > 0).subscribe(v => {
    //   this.authenticated;
    // });
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet.activatedRouteData.animation || 'one';
  }
}
